//
//  MainViewController.m
//  Gadgets
//
//  Created by Craig on 7/11/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import "MainViewController.h"
#import "Sockets.h"


@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(draw:)
                                                 name:@"update"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Host"];
    NSArray *hosts = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (int i = 0; i < [hosts count]; i++) {
        NSManagedObject *aHost = [hosts objectAtIndex:i];
        if ([[aHost valueForKey:@"selected"] boolValue]) {
            if (aHost != host) {
                [self clear];
                host = aHost;
            }
        }
    }
    if (host != nil) {
        [self connectToHost];
    } else {
        //alert user?
    }
}

-(void)clear {
    if (sockets != nil && [sockets isRunning]) {
        [sockets stop];
        [[scroll subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [sockets release];
        [commands release];
        [devices release];
        sockets = nil;
        host = nil;
    }
}

//Update the screen
- (void)draw:(NSNotification *)notification {
    if (paused == NO) {
        NSDictionary *data = (NSDictionary *)[notification object];
        [self performSelectorOnMainThread:@selector(drawData:) withObject:data waitUntilDone:NO];
    }
}

- (void)drawData:(NSDictionary *)data {
    NSDictionary *locationsData = [data objectForKey:@"locations"];
    NSDictionary *location;
    NSArray* keys = [locationsData allKeys];
    for (NSString *locationName in keys) {
        location = [locationsData objectForKey:locationName];
        NSDictionary *input = [location objectForKey:@"input"];
        NSDictionary *output = [location objectForKey:@"output"];
        for (id key in input) {
            NSLog(@"Key: %@", key);
            UILabel *label = [devices objectForKey:[NSString stringWithFormat:@"%@ %@", locationName, key]];
            NSNumber *value = [[input objectForKey:key] objectForKey:@"value"];
            [self updateLabel:label withValue:value];
        }
        for (id key in output) {
            NSLog(@"Key: %@", key);
            UIButton *button = [devices objectForKey:[NSString stringWithFormat:@"%@ %@", locationName, key]];
            NSNumber *value = [[output objectForKey:key] objectForKey:@"value"];
            [self updateButton:button withValue:value];
        }
    }
}

-(void) updateButton:(UIButton *)button withValue:(NSNumber *)value {
    if (button.selected != [value intValue]) {
        button.selected = !button.selected;
    }
}

-(void) updateLabel:(UILabel *)label withValue:(NSNumber *)value {
    if ([value floatValue] == 1.0 || [value floatValue] == 0.0) {
        [self updateBool:label withValue:value];
    } else {
        [self updateFloat:label withValue:value];
    }
}

-(void) updateBool:(UILabel *)label withValue:(NSNumber *)value {
    if ([value boolValue] == YES) {
        label.text = @"True";
    } else {
        label.text = @"False";
    }
}

-(void) updateFloat:(UILabel *)label withValue:(NSNumber *)value {
    NSString *val = [NSString stringWithFormat:@"%.2f", [value doubleValue]];
    label.text = val;
}


//Add the UI elements to the screen
- (void)drawInitialData {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self drawInitial];
    });
}

- (void)drawInitial {
    NSMutableDictionary *data = [sockets getInitialData];
    events = [data objectForKey:@"events"];
    NSDictionary *initialData = [data objectForKey:@"status"];
    NSArray *tmp = [[initialData objectForKey:@"locations"] allKeys];
    locations = [NSArray arrayWithArray:[tmp sortedArrayUsingComparator:^(NSString* a, NSString* b) {
        return [a compare:b options:NSNumericSearch];
    }]];
    [self addRows:initialData];
}

- (void) addRows:(NSDictionary *)data {
    NSDictionary *locationsData = [data objectForKey:@"locations"];
    NSDictionary *location;
    commands = [[NSMutableArray alloc] init];
    devices = [[NSMutableDictionary alloc] init];
    int row = 0;
    for (int i = 0; i < [locations count]; i++) {
        NSString *locationName = [locations objectAtIndex:i];
        location = [locationsData objectForKey:locationName];
        row += 1;
        [self addLabel:locationName atRow:row andCol:10];
        NSDictionary *input = [location objectForKey:@"input"];
        NSDictionary *output = [location objectForKey:@"output"];
        for (id key in input) {
            row += 1;
            UILabel *label = [self addInput:key atLocation:[input objectForKey:key] atRow:row withLocationName:locationName];
            [devices setObject:label forKey:[NSString stringWithFormat:@"%@ %@", locationName, key]];
        }
        for (id key in output) {
            row += 1;
            UIButton *button = [self addOutput:key atLocation:[output objectForKey:key] atRow:row withLocationName:locationName];
            [devices setObject:button forKey:[NSString stringWithFormat:@"%@ %@", locationName, key]];
        }
    }
}

- (UILabel *) addInput:(NSString *)name atLocation:(NSMutableDictionary *)data atRow:(int)row withLocationName:(NSString *)locationName {
    [self addLabel:name atRow:row andCol:30];
    id value = [data objectForKey:@"value"];
    NSNumber *val = (NSNumber *)value;
    UILabel *label;
    if ([val floatValue] == 1.0 || [val floatValue] == 0.0) {
        label = [self addBool:val atRow:row];
    } else  {
        label = [self addFloat:val atRow:row];
    }
    return label;
 }

- (UILabel *)addBool:(NSNumber *)value atRow:(int)row {
    NSString *val;
    if ([value boolValue] == YES) {
        val = @"True";
    } else {
        val = @"False";
    }
    return [self addLabel:val atRow:row andCol:150];
}

- (UILabel *)addFloat:(NSNumber *)value atRow:(int)row {
    NSString *val = [NSString stringWithFormat:@"%.2f", [value doubleValue]];
    return [self addLabel:val atRow:row andCol:150];
}

- (UIButton *) addOutput:(NSString *)name atLocation:(NSMutableDictionary *)data atRow:(int)row withLocationName:(NSString *)locationName {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame:CGRectMake(150, (row * 30) + 20, 30, 30)];
    [button setImage:[UIImage imageNamed:@"off.jpeg"] forState:UIControlStateNormal];
    [button setImage: [UIImage imageNamed:@"on.jpeg"] forState:UIControlStateSelected];
    NSDictionary *c = [[events objectForKey:locationName] objectForKey:name];
    [button setTag:[commands count]];
    [commands addObject:c];
    if ([[data objectForKey:@"value"] intValue] == 0) {
        [button setSelected:0];
    }
    else{
        [button setSelected:1];
    }
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(doPrompt:)];
    longPress.cancelsTouchesInView = YES;
    [longPress setMinimumPressDuration:1];
    [button addGestureRecognizer:longPress];
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addLabel:name atRow:row andCol:30];
    [self performSelectorOnMainThread:@selector(setButton:) withObject:button waitUntilDone:YES];
    [longPress release];
    return button;
}

-(void)buttonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSString *command = [self getCommand:button];
    NSArray *commandArray = [NSArray arrayWithObjects:command, @"{}", nil];
    NSLog(@"command: %@", command);
    [sockets send:commandArray];
}

- (UILabel *)addLabel:(NSString *)name atRow:(int)row andCol:(int)col {
    UILabel *label =  [[UILabel alloc] initWithFrame: CGRectMake(col, (row * 30) + 20, 120, 30)];
    label.backgroundColor = [UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:1.0];
    label.textColor = [UIColor colorWithRed:88/255.f green:249/255.f blue:88/255.f alpha:1.0];
    label.text = name;
    [self performSelectorOnMainThread:@selector(setLabel:) withObject:label waitUntilDone:YES];
    return label;
}

- (void)setButton:(UIButton *)button {
    [scroll addSubview:button];
}

- (void)setLabel:(UILabel *)label {
    [scroll addSubview:label];
}



- (NSString *)getCommand:(UIButton *)button {
    NSString *command;
    if (button.selected == YES) {
        command = [[commands objectAtIndex:button.tag] objectForKey:@"off"];
    }
    else {
        command = [[commands objectAtIndex:button.tag] objectForKey:@"on"];
    }
    return command;
}


- (void) connectToHost {
    if (sockets == nil) {
        sockets = [[Sockets alloc] initWithHost:(Host *)host];
        sleep(1);
        [self drawInitialData];
        paused = NO;
        [sockets start];
    }
    else {
        [sockets close];
        [sockets release];
        sockets = nil;
        [self connectToHost];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    }
    paused = NO;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.flipsidePopoverController = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            UIPopoverController *popoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            self.flipsidePopoverController = popoverController;
            popoverController.delegate = self;
        }
        paused = YES;
    }
}

- (void)dealloc
{
    [_managedObjectContext release];
    [_flipsidePopoverController release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [commands release];
    [devices release];
    [sockets close];
    [sockets release];
    [super dealloc];
}

- (IBAction)togglePopover:(id)sender
{
    if (self.flipsidePopoverController) {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    } else {
        [self performSegueWithIdentifier:@"showAlternate" sender:sender];
    }
}

@end
