//
//  Host.h
//  Gadgets
//
//  Created by Craig on 7/12/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Host : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * publishPort;
@property (nonatomic, retain) NSString * subscribePort;
@property (nonatomic, retain) NSString * requestPort;
@property (nonatomic, retain) NSNumber * selected;

-(NSString *)getSubscribeURL;
-(NSString *)getPublishURL;
-(NSString *)getRequestURL;


@end
