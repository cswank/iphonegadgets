//
//  MainViewController.h
//  Gadgets
//
//  Created by Craig on 7/11/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import "FlipsideViewController.h"
#import "Sockets.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate, UIPopoverControllerDelegate> {
    NSManagedObject *host;
    NSArray *locations;
    Sockets *sockets;
    NSDictionary *events;
    NSMutableDictionary *devices;
    NSMutableArray *commands;
    IBOutlet UIScrollView *scroll;
    Boolean paused;
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;

@end
