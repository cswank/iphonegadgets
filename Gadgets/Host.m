//
//  Host.m
//  Gadgets
//
//  Created by Craig on 7/12/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import "Host.h"


@implementation Host

@dynamic address;
@dynamic name;
@dynamic publishPort;
@dynamic subscribePort;
@dynamic requestPort;
@dynamic selected;

-(NSString *)getSubscribeURL {
    return [NSString stringWithFormat:@"tcp://%@:%@", self.address, self.subscribePort];
}

-(NSString *)getRequestURL {
    return [NSString stringWithFormat:@"tcp://%@:%@", self.address, self.requestPort];
}

-(NSString *)getPublishURL {
    return [NSString stringWithFormat:@"tcp://%@:%@", self.address, self.publishPort];
}

@end
