//
//  FlipsideViewController.h
//  Gadgets
//
//  Created by Craig on 7/11/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

@interface FlipsideViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *hostsTableView;
    NSManagedObject *selectedHost;
}

@property (assign, nonatomic) id <FlipsideViewControllerDelegate> delegate;
@property (strong) NSMutableArray *hosts;

- (IBAction)done:(id)sender;
- (NSManagedObject *)getHost;

@end
