//
//  Sockets.h
//  Gadgets
//
//  Created by Craig on 7/12/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMQObjC.h"
#import "Host.h"

@interface Sockets : NSObject {
    Host *host;
    ZMQContext *context;
    ZMQSocket *publisher;
    NSDictionary *events;
    Boolean run;
}

- (void)start;
- (void)stop;
- (Boolean)isRunning;
-(id)initWithHost:(Host *)aHost;
- (void)send:(NSArray *)data;
- (NSMutableDictionary *) getInitialData;
-(void)close;

@end
