//
//  FlipsideViewController.m
//  Gadgets
//
//  Created by Craig on 7/11/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import "FlipsideViewController.h"
#import "HostViewController.h"

@interface FlipsideViewController ()

@end

@implementation FlipsideViewController

- (void)awakeFromNib
{
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //hostsTableView.dataSource = self;
    [hostsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Host"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Host"];
    self.hosts = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"hosts: %@", self.hosts);
    [hostsTableView reloadData];
    for (int i = 0; i < [self.hosts count]; i++) {
        NSManagedObject *host = [self.hosts objectAtIndex:i];
        if ([[host valueForKey:@"selected"] boolValue]) {
            NSLog(@"yes");
            selectedHost = host;
            NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
            [hostsTableView selectRowAtIndexPath:path animated:NO scrollPosition:0];
        } else {
            NSLog(@"no");
        }
    }
    
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.hosts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Host";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSManagedObject *host = [self.hosts objectAtIndex:indexPath.row];
    [cell.textLabel setText:[host valueForKey:@"name"]];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    HostViewController *destViewController = (HostViewController *)segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"editHost"]) {
        NSIndexPath *indexPath = [hostsTableView indexPathForSelectedRow];
        NSLog(@"index path: %@", indexPath);
        if (indexPath != NULL) {
            NSManagedObjectContext *context = [self managedObjectContext];
            [destViewController setHost:[self.hosts objectAtIndex:indexPath.row] withContext:context];
        }
    } else if ([segue.identifier isEqualToString:@"newHost"]) {
        // Create a new managed object
        NSManagedObjectContext *context = [self managedObjectContext];
        NSLog(@"context: %@", context);
        NSManagedObject *newHost = [NSEntityDescription insertNewObjectForEntityForName:@"Host" inManagedObjectContext:context];
        [newHost setValue:@"6111" forKey:@"subscribePort"];
        [newHost setValue:@"6112" forKey:@"publishPort"];
        [destViewController setHost:newHost withContext:context];
    }
}

- (void)hostViewControllerDidFinish:(HostViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        //[hostPopoverController dismissPopoverAnimated:YES];
        //hostPopoverController = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSManagedObject *)getHost {
    return selectedHost;
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    NSIndexPath *indexPath = [hostsTableView indexPathForSelectedRow];
    if (indexPath != NULL) {
        NSManagedObjectContext *context = [self managedObjectContext];
        for (int i = 0; i < [self.hosts count]; i++) {
            NSManagedObject *host = [self.hosts objectAtIndex:i];
            if (indexPath.row == i) {
                [host setValue:[NSNumber numberWithBool:YES] forKey:@"selected"];
                selectedHost = host;
            } else {
                [host setValue:[NSNumber numberWithBool:NO] forKey:@"selected"];
            }
            NSError *error = nil;
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            }
        }
    }
    [self.delegate flipsideViewControllerDidFinish:self];
}

@end
