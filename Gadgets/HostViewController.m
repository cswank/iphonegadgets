//
//  HostViewController.m
//  Gadgets
//
//  Created by Craig on 7/11/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import "HostViewController.h"

@interface HostViewController ()

@end

@implementation HostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = [host valueForKey:@"name"];
    nameField.text = [host valueForKey:@"name"];
    addressField.text = [host valueForKey:@"address"];
    pubField.text = [host valueForKey:@"publishPort"];
    subField.text =[host valueForKey:@"subscribePort"];
    reqField.text =[host valueForKey:@"requestPort"];
}

-(void)setHost:(NSManagedObject *)aHost withContext:(NSManagedObjectContext *)ctx {
    context = ctx;
    host = aHost;
}



- (IBAction)save:(id)sender {
    [host setValue:nameField.text forKey:@"name"];
    [host setValue:addressField.text forKey:@"address"];
    [host setValue:subField.text forKey:@"subscribePort"];
    [host setValue:pubField.text forKey:@"publishPort"];
    [host setValue:reqField.text forKey:@"requestPort"];
    
    NSError *error = nil;
    NSLog(@"context: %@", context);
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
