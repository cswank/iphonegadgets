//
//  Sockets.m
//  Gadgets
//
//  Created by Craig on 7/12/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import "Sockets.h"
#import "SBJson.h"

@implementation Sockets

-(id)initWithHost:(Host *)aHost {
    host = aHost;
    context = [[ZMQContext alloc] initWithIOThreads:4U];
    publisher = [context socketWithType:ZMQ_PUB];
    if (![publisher connectToEndpoint:[host getPublishURL]]) {
        /* ZMQSocket will already have logged the error. */
    }
    return [super init];
}

-(void)start {    
    [self performSelectorInBackground:@selector(getData) withObject:self];
}

-(void)close {
    [publisher close];
    [context closeSockets];
}

- (void)send:(NSArray *)data {
    [publisher sendMultipart:data];
}

- (NSMutableDictionary *) getInitialData {
    NSString *statusEvent = @"status";
    NSString *commandsEvent = @"events";
    
    NSString *msg = @"{\"id\": \"ipad\"}";
    
    ZMQSocket *req = [context socketWithType:ZMQ_REQ];
    if (![req connectToEndpoint:[host getRequestURL]]) {
        /* ZMQSocket will already have logged the error. */
        NSLog(@"couldn't connect to sub url %@", [host getSubscribeURL]);
    }
    
    NSArray *b = [NSArray arrayWithObjects:commandsEvent, msg, nil];
    [req sendMultipart:b];
    [req recv]; //id comes first
    NSLog(@"got it");
    events = [[req recv] JSONValue];
    
    NSArray *a = [NSArray arrayWithObjects:statusEvent, msg, nil];
    [req sendMultipart:a];
    [req recv]; //id comes first
    NSMutableDictionary *d = [[[req recv] JSONValue] mutableCopy];
    [req close];
    NSMutableDictionary *dict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:events, d, nil] forKeys:[NSArray arrayWithObjects:@"events", @"status", nil]];
    return dict;
}

- (void)stop {
    run = NO;
    sleep(1);
}

- (Boolean)isRunning {
    return run;
}

- (void)getData {
    ZMQSocket *subscriber = [context socketWithType:ZMQ_SUB];
    if (![subscriber connectToEndpoint:[host getSubscribeURL]]) {
        /* ZMQSocket will already have logged the error. */
    }
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    NSString *subscription = @"UPDATE";
    NSData *data = [subscription dataUsingEncoding:NSUTF8StringEncoding];
    [subscriber setData:data forOption:ZMQ_SUBSCRIBE];
    
    
    zmq_pollitem_t items[1];
    [subscriber getPollItem:&items[0] forEvents:ZMQ_POLLIN];
    run = YES;
    while (run == YES) {
        NSAutoreleasePool *p = [[NSAutoreleasePool alloc] init];
        int ret = [ZMQContext pollWithItems:items count:1 timeoutAfterUsec:1000];
        if (ret > 0) {
            NSString *message = [subscriber recv];
            if (![message isEqualToString:subscription]) {
                NSDictionary *data = [message JSONValue];
                [center postNotificationName:@"update" object:data];
            }
        [p drain];
        }
    }
    [context closeSockets];
}

@end
