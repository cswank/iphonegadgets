//
//  HostViewController.h
//  Gadgets
//
//  Created by Craig on 7/11/13.
//  Copyright (c) 2013 Craig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Host.h"

@interface HostViewController : UIViewController {
    IBOutlet UITextField *nameField;
    IBOutlet UITextField *addressField;
    IBOutlet UITextField *subField;
    IBOutlet UITextField *pubField;
    IBOutlet UITextField *reqField;
    NSManagedObject *host;
    NSManagedObjectContext *context;
}

-(void)setHost:(NSManagedObject *)aHost withContext:(NSManagedObjectContext *)ctx;
- (IBAction)cancel:(id)sender;
- (IBAction)save:(id)sender;

@end
