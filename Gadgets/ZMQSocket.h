#import <Foundation/Foundation.h>
#import "zmq.h"

#define BUFSIZE 1024 * 4

@class ZMQContext;

typedef int ZMQSocketType;
typedef int ZMQSocketOption;
typedef int ZMQMessageSendFlags;
typedef int ZMQMessageReceiveFlags;

@interface ZMQSocket : NSObject {
	void *socket;
    char buf[BUFSIZE];
	ZMQContext *context;  // not retained
	NSString *endpoint;
	ZMQSocketType type;
	BOOL closed;
}
// Returns @"ZMQ_PUB" for ZMQ_PUB, for example.
+ (NSString *)nameForSocketType:(ZMQSocketType)type;

// Create a socket using -[ZMQContext socketWithType:].
@property(readonly, assign, NS_NONATOMIC_IPHONEONLY) ZMQContext *context;
@property(readonly, NS_NONATOMIC_IPHONEONLY) ZMQSocketType type;

- (void)close;
// KVOable.
@property(readonly, getter=isClosed, NS_NONATOMIC_IPHONEONLY) BOOL closed;
@property(readonly, copy, NS_NONATOMIC_IPHONEONLY) NSString *endpoint;

#pragma mark Socket Options
- (BOOL)setData:(NSData *)data forOption:(ZMQSocketOption)option;
- (NSData *)dataForOption:(ZMQSocketOption)option;

#pragma mark Endpoint Configuration
- (BOOL)bindToEndpoint:(NSString *)endpoint;
- (BOOL)connectToEndpoint:(NSString *)endpoint;

#pragma mark Communication
- (int)send:(NSString *)message;
- (int)send:(NSString *)message withFlag:(int)flag;
- (int)sendMultipart:(NSArray *)messages;
- (NSString *)recv;
- (NSString *)recvWithFlag:(int)flag;

#pragma mark Polling
- (void)getPollItem:(zmq_pollitem_t *)outItem forEvents:(short)events;
@end
